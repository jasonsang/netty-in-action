package nia.chapter4;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.oio.OioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.oio.OioServerSocketChannel;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 * 使用 Netty 的阻塞网络处理
 * Listing 4.3 Blocking networking with Netty
 *
 * @author <a href="mailto:norman.maurer@gmail.com">Norman Maurer</a>
 */
public class NettyOioServer {
    public void server(int port) throws Exception {
        final ByteBuf buf = Unpooled.unreleasableBuffer(Unpooled.copiedBuffer("Hi!\r\n", Charset.forName("UTF-8")));
        EventLoopGroup group = new OioEventLoopGroup();
        try {
            /*创建SeverBootStrap*/
            ServerBootstrap b = new ServerBootstrap();
            /*使用OioEventLoopGroup以允许阻塞模式（旧的I/O）*/
            b.group(group).channel(OioServerSocketChannel.class)
                    .localAddress(new InetSocketAddress(port))
                    /*指定ChannelInitializer对于每个已接受的连接都调用它*/
//                    .childHandler(new ChannelInitializer<SocketChannel>() {
//                        @Override
//                        public void initChannel(SocketChannel ch)
//                                throws Exception {
//                                ch.pipeline().addLast(
//                                    new ChannelInboundHandlerAdapter() {
//                                        @Override
//                                        public void channelActive(
//                                                ChannelHandlerContext ctx)
//                                                throws Exception {
//                                            ctx.writeAndFlush(buf.duplicate())
//                                                    .addListener(
//                                                            ChannelFutureListener.CLOSE);
//                                        }
//                                    });
//                        }
//                    });
                    /*指定channelInitializer,对于每个已接受的连接都调用它*/
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            /*添加一个ChannelInboundHandlerAdapter以拦截和处理事件*/
                            ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                                @Override
                                public void channelActive(ChannelHandlerContext ctx) throws Exception {
//                                    super.channelActive(ctx);

/*将消息写到客户端，并添加ChannelFutureListener，以便消息一被写完就关闭连接*/
                                    ctx.writeAndFlush(buf.duplicate()).addListener(ChannelFutureListener.CLOSE);
                                }
                            });
                        }
                    });
            /*绑定服务器以接受连接*/
            ChannelFuture f = b.bind().sync();
            f.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully().sync();
        }
    }
}

