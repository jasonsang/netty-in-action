package cxzTest;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.CharsetUtil;
import io.netty.util.HashedWheelTimer;
import io.netty.util.concurrent.DefaultThreadFactory;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
public class cxzTestServer {
//    private static  Channel channel;
    private static int retryCount = 0;
//    private static Bootstrap bootstrap = new Bootstrap();
    private static EventLoopGroup group = new NioEventLoopGroup(2);
    private static final String ip = "10.2.176.224";
    private static final int port = 62937;
    private  static ExecutorService executorService =newFixedThreadPool(1000);
    protected HashedWheelTimer timer = new HashedWheelTimer();
    private static Bootstrap bootstrap;


    public static void main(String[] args) throws InterruptedException {
        // initClientPool(100);
            bootstrap = new Bootstrap();
            initClientPool(bootstrap);
    }

    public static String getCode(int finalI){
        String code = "";
        if (finalI == 0) {
            code = "<ncda><req>yes</req><st>nc1</st>null<var>mode,status,ncstate,prg,block,proctimestart,proctimeproc,remaindertime</var></ncda>\n";
        } else if (finalI == 1) {
            code = "<spindle><req>yes</req></spindle>\n";
        } else if (finalI == 2) {
            code = "<blocks><req>yes</req></blocks>\n";
        }
        else if (finalI == 3) {
            code = "<gcodes><req>yes</req></gcodes>\n";
        } else if (finalI == 4) {
            code = "<bcd><req>yes</req></bcd>\n";
        } else if (finalI == 5) {
            code = "<cncvar><req>yes</req><id>PEW4190.1</id></cncvar>\n";
        } else if (finalI == 6) {
            code = "<names><req>yes</req><st>nc1</st></names>\n";
        } else if (finalI == 7) {
            code = "<axes><req>yes</req><auto>yes</auto><sub>offset</sub></axes>\n";
        } else if (finalI == 8) {
            code = "<feed><req>yes</req></feed>\n";
        } else if (finalI == 9) {
            code = "<sharpvar><req>yes</req><auto>yes</auto><id>451</id></sharpvar>\n";
        }
        return  code;
    }

    public static void initClientPool(Bootstrap bootstrap) throws InterruptedException {
        try {
            // 连接超时时间
//            bootstrap.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000);
            // 心跳
//            bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
            // 配置Channel参数，nodelay没有延迟，true就代表禁用Nagle算法，减小传输延迟。
//            bootstrap.option(ChannelOption.TCP_NODELAY, true);
            bootstrap.option(ChannelOption.SO_TIMEOUT, 5000);

            bootstrap.group(group).channel(NioSocketChannel.class)
                    .remoteAddress(new InetSocketAddress(ip, port)) // 绑定连接端口和host信息
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new IdleStateHandler(1, 0, 0));
                            ch.pipeline().addLast(new StringEncoder(Charset.forName("GBK")));
                            ch.pipeline().addLast(new cxzTestHandle(getCode(0),bootstrap,new HashedWheelTimer()));/*返回结果*/
                            ch.pipeline().addLast(new ByteArrayEncoder());
                            ch.pipeline().addLast(new ChunkedWriteHandler());
                            ch.pipeline().addLast( new ConnectorIdleStateTrigger(getCode(0)));
                        }
                    });
//        doConnect(bootstrap);
            myConnect(bootstrap.register().channel());
        } catch (Throwable throwable) {
            log.error(throwable.getMessage());
        }

    }

    protected static void myConnect(Channel channel) throws InterruptedException {

        if (channel != null && channel.isActive()) {
            return;
        } else {
            ChannelFuture future = bootstrap.connect().sync(); // 异步连接服务器
            //System.out.println("服务端连接成功..."); // 连接完成
            future.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture futureListener) throws Exception {
                    if (futureListener.isSuccess()) {
                        /*channel = (SocketChannel) */
                        futureListener.channel();

                    } else {
                        System.out.println("重连");
                        futureListener.channel().eventLoop().schedule(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    myConnect(channel);
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        }, 10, TimeUnit.SECONDS);
                    }
                }
            });
            future.channel().closeFuture().sync(); // 异步等待关闭连接channel
        }
    }
//
//    public static void doConnect(Bootstrap bootstrap){
//        ChannelFuture f = null;
//        try {
//            f = bootstrap.connect().sync();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        Channel channel = f.channel();
//        f.addListener(new ChannelFutureListener() {
//
//            public void operationComplete(ChannelFuture future) throws Exception {
//                if (future.isSuccess()) {
//                    System.out.println("连接成功");
//
//                } else {
//                    System.out.println("连接失败");
//                }
//            }
//        });
//
////        while(true){
////            try {
////                System.out.println(channel.isActive());
////                if(!channel.isActive()){
////                    channel = bootstrap.connect().sync().channel();
////                }
////
////                TimeUnit.SECONDS.sleep(1);
////                channel.writeAndFlush(Unpooled.copiedBuffer(getCode(finalI), CharsetUtil.UTF_8));
////            } catch (InterruptedException e) {
////                e.printStackTrace();
////            }
////        }
//    }


    public static void ceshi(String code,Bootstrap bootstrap,Channel channel) throws InterruptedException {
        final Channel[] channel1 = {channel};
        executorService.execute(()->{
            while(true){

                try {
                    if(!channel1[0].isActive()){
                        channel1[0] = bootstrap.connect().sync().channel();
                    }

                    TimeUnit.SECONDS.sleep(3);
                    channel.writeAndFlush(Unpooled.copiedBuffer(code, CharsetUtil.UTF_8));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public static ExecutorService newFixedThreadPool(int nThreads) {
        return new ThreadPoolExecutor(nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }
}

