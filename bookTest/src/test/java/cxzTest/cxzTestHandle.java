package cxzTest;


import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.*;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

@Slf4j
public class cxzTestHandle extends SimpleChannelInboundHandler<ByteBuf> implements TimerTask {
    /*发送信息指令*/
    private String toCode;
    private Bootstrap bootstrap;
    private boolean reconnect = true;
    private int attempts;
    private  Timer timer;
    public cxzTestHandle(String toCode,Bootstrap bootstrap,Timer timer) {
        this.toCode = toCode;
        this.bootstrap = bootstrap;
        this.timer = timer;
    }
    /**
     * 向服务端发送数据
     */

    @Override
    public void channelActive(ChannelHandlerContext ctx)  {
        System.out.println("客户端与服务端通道-开启：" + ctx.channel().localAddress() + "channelActive");
//        toCode = "<alarm><req>yes</req><auto>yes</auto></alarm>\n";
        //System.out.println("客户端准备发送的数据包：" + toCode);
//        attempts = 0;
        ctx.fireChannelActive();
        // ctx.writeAndFlush(Unpooled.copiedBuffer(toCode, CharsetUtil.UTF_8)); // 必须有flush
    }

    /**
     * channelInactive
     * <p>
     * channel 通道 Inactive 不活跃的
     * <p>
     * 当客户端主动断开服务端的链接后，这个通道就是不活跃的。也就是说客户端与服务端的关闭了通信通道并且不可以传输数据
     */
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("客户端与服务端通道-关闭："
                + ctx.channel().localAddress()
                + "channelInactive");

//        if(reconnect){
//            System.out.println("链接关闭，将进行重连");
//            if (attempts < 12) {
//                attempts++;
//                //重连的间隔时间会越来越长
//                int timeout = 2 << attempts;
//                timer.newTimeout(this, timeout, TimeUnit.MILLISECONDS);
//            }
//        }
        while (attempts < 12){
            new cxzTestServer();
            attempts++;
//                //重连的间隔时间会越来越长
                int timeout = 2 << attempts;
                timer.newTimeout(this, timeout, TimeUnit.MILLISECONDS);
            cxzTestServer.myConnect(ctx.channel());

        }

        ctx.fireChannelInactive();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        ByteBuf buf = msg.readBytes(msg.readableBytes());
        /**System.out.println(
         "客户端接收到的服务端信息:"
         + ByteBufUtil.hexDump(buf)
         + "; 数据包为:"
         + buf.toString(Charset.forName("gbk")));*//*解析返回值*/
        Document doc = Jsoup.parse(buf.toString(Charset.forName("gbk")));//html为内容
        System.out.println("处理时间："+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())+";接收数据："+doc.toString());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        ctx.close();
        System.out.println("异常退出:" + cause.getMessage());
        channelInactive(ctx);
//        new cxzTestServer().initClientPool(bootstrap);
    }

    @Override
    public void run(Timeout timeout) throws Exception {
        System.out.println("调用重连方法");
        new cxzTestServer().initClientPool(bootstrap);
    }
}
