import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

import java.lang.ref.SoftReference;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Test {
    public static void main(String[] args) throws InterruptedException {
//        ByteBuf buf = PooledByteBufAllocator.DEFAULT.buffer(1024);
        System.gc();
        Thread.sleep(TimeUnit.MILLISECONDS.toMillis(1000));
        PooledByteBufAllocator.DEFAULT.buffer(1024);
        /*软引用*/
        SoftReference<Date> reference = new SoftReference<>(new Date());
        Date date = reference.get();
    }
}
