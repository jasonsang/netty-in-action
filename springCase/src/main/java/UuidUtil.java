import java.util.UUID;

/**
 * uuid工具类
 *
 * @author wangning40
 * @date 2019-05-05
 **/
public class UuidUtil {
  /**
   * 获得随机32位uuid
   *
   * @return
   */
  public static String get32Uuid() {
    String uuid = UUID.randomUUID().toString().trim().replaceAll("-","");
    return uuid;
  }
}