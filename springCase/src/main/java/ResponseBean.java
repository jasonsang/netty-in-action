

import java.util.HashMap;

public class ResponseBean extends HashMap {
    private String code = "10000";
    private String message;
    private long count;
    private int totalPage;
    private Object results;

    public ResponseBean() {
        this.setMessage("success");
        this.setCode("10000");
        this.setCount(0L);
//        this.setResults("");
    }

    public int getTotalPage() {
        return this.totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.put("totalPage", Integer.valueOf(totalPage));
        this.totalPage = totalPage;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.put("message", message);
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.put("code", code);
        this.code = code;
    }

    public long getCount() {
        return this.count;
    }

    public void setCount(long count) {
        this.put("count", Long.valueOf(count));
        this.count = count;
    }

    public Object getResults() {
        return this.results;
    }

    public void setResults(Object results) {
        this.put("results", results);
        this.results = results;
    }

    public void put(String key, Object value) {
        super.put(key, value);
    }
}
