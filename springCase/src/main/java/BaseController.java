import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.beans.PropertyEditorSupport;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;

/**
 *
 * @ClassName: BaseController
 * @Description: 基本 Controller
 * @author: tomy
 * @Date: 2021/4/12 18:01
 */
@Slf4j
public abstract class BaseController {

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                try {
                    setValue(DateUtils.parseDate(text));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    /**
     * 使用response输出JSON
     *
     * @param response response
     * @param str      响应对应
     */
    protected void out(HttpServletResponse response, Object str) {
        PrintWriter out = null;
        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json");
            out = response.getWriter();
            out.print(str);
        } catch (Exception e) {
            log.error("输出JSON出错", e.getMessage());
        } finally {
            if (out != null) {
                out.flush();
                out.close();
            }
        }
    }

    /**
     * 判断参数 是否存在
     *
     * @param responseBean responseBean
     * @param jsonParam 需要判断的原始参数
     * @param params 需要判断的key
     * @return
     */
    protected boolean getParams(ResponseBean responseBean, JSONObject jsonParam, String[] params) {
        boolean flag = false;
        for (String param : params) {
            if (jsonParam.containsKey(param)) {
                if (StringUtils.isNotBlank(String.valueOf(jsonParam.get(param)))) {
                    flag = true;
                } else {
                    flag = false;
                    break;
                }
            } else {
                flag = false;
                break;
            }
        }
        if (!flag) {
            responseBean.setResults(false);
            responseBean.setCode("40003");
            responseBean.setMessage("缺少参数&参数不能为空");
        }
        return flag;
    }
	/**
	 * 获取ModelAndView
	 */
	public ModelAndView getModelAndView(){
		return new ModelAndView();
	}

/**
	 * 获取request对象
	 */
	public HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return request;
	}
	
	/**
	 * 获取项目在服务器中的文件位置
	 */
	
	public String getWebRoot(){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		return request.getServletContext().getRealPath("/").replace("\\", "/");
	}

	/**
	 * 获取32位的uuid
	 * @return
	 */
	public String get32UUID(){
		
		return UuidUtil.get32Uuid();
	}
	
	/**
	 * 获取客户端Ip
	 * 
	 * @return ip
	 */
	public String getClientIp(){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		 if (request.getHeader("x-forwarded-for") == null) {  
	        return request.getRemoteAddr();  
	    }  
	    return request.getHeader("x-forwarded-for");  
	}
	/**
	 * 判断请求来源
	 * 
	 * @return 结果
	 */
	public String getTerminal(){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		String terminal = request.getHeader("User-Agent");
		if(terminal.contains("Windows NT")){
            terminal = "pc";
        }else{
            terminal = "mobile";
        }
		return terminal;
	}
}
