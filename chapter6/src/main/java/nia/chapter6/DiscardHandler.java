package nia.chapter6;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

/**
 * Listing 6.1 Releasing message resources
 *
 * @author <a href="mailto:norman.maurer@gmail.com">Norman Maurer</a>
 */
@Sharable
/*Netty 定义了下面两个重要的 ChannelHandler 子接口：
 ChannelInboundHandler——处理入站数据以及各种状态变化；
 ChannelOutboundHandler——处理出站数据并且允许拦截所有的操作*/
public class DiscardHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        /*丢弃已接收的消息*/
        ReferenceCountUtil.release(msg);
    }

}

