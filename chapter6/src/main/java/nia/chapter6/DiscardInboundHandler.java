package nia.chapter6;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

/**
 * Listing 6.3 Consuming and releasing an inbound message
 *
 * @author <a href="mailto:norman.maurer@gmail.com">Norman Maurer</a>
 */
@Sharable
public class DiscardInboundHandler extends ChannelInboundHandlerAdapter {

    /*ChannelHandlerContext 代表了 ChannelHandler 和 ChannelPipeline 之间的关联，
    每当有 ChannelHandler 添加到 ChannelPipeline 中时，都会创建 ChannelHandlerContext。
    ChannelHandlerContext 的主要功能是管理它所关联的 ChannelHandler 和
    在同一个 ChannelPipeline 中的其他 ChannelHandler 之间的交互。*/
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        ReferenceCountUtil.release(msg);
    }
}
