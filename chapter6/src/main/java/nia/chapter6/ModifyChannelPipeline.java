package nia.chapter6;

import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelPipeline;

import static io.netty.channel.DummyChannelPipeline.DUMMY_INSTANCE;


/**
 * Listing 6.5 Modify the ChannelPipeline
 *
 * @author <a href="mailto:norman.maurer@gmail.com">Norman Maurer</a>
 */
public class ModifyChannelPipeline {
    private static final ChannelPipeline pipeline = DUMMY_INSTANCE;

    /**
     * Listing 6.5 Modify the ChannelPipeline
     * */
    public static void modifyPipeline() {


        FirstHandler firstHandler = new FirstHandler();

//        ChannelPipeline pipeline = new DummyChannelPipeline();
        pipeline.addLast("handler1", firstHandler);
        /*将一个 SecondHandler的实例作为"handler2"
        添加到 ChannelPipeline的第一个槽中。
        这意味着它将被放置在已有的"handler1"之前*/
        pipeline.addFirst("handler2", new SecondHandler());
        pipeline.addLast("handler3", new ThirdHandler());
        //...
        pipeline.remove("handler3");
        pipeline.remove(firstHandler);
        /*后替换前*/
        pipeline.replace("handler2", "handler4", new FourthHandler());

        System.out.println(pipeline.get("handler2"));
    }

    public static void main(String[] args) {
        modifyPipeline();
    }
    private static final class FirstHandler
        extends ChannelHandlerAdapter {

    }

    private static final class SecondHandler
        extends ChannelHandlerAdapter {

    }

    private static final class ThirdHandler
        extends ChannelHandlerAdapter {

    }

    private static final class FourthHandler
        extends ChannelHandlerAdapter {

    }
}
