package nia.chapter5;

import io.netty.buffer.*;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.ByteProcessor;
import io.netty.util.ReferenceCounted;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Random;

import static io.netty.channel.DummyChannelHandlerContext.DUMMY_INSTANCE;

/**
 * Created by kerr.
 * <p>
 * Listing 5.1 Backing array
 * <p>
 * Listing 5.2 Direct buffer data access
 * <p>
 * Listing 5.3 Composite buffer pattern using ByteBuffer
 * <p>
 * Listing 5.4 Composite buffer pattern using CompositeByteBuf
 * <p>
 * Listing 5.5 Accessing the data in a CompositeByteBuf
 * <p>
 * Listing 5.6 Access data
 * <p>
 * Listing 5.7 Read all data
 * <p>
 * Listing 5.8 Write data
 * <p>
 * Listing 5.9 Using ByteBufProcessor to find \r
 * <p>
 * Listing 5.10 Slice a ByteBuf
 * <p>
 * Listing 5.11 Copying a ByteBuf
 * <p>
 * Listing 5.12 get() and set() usage
 * <p>
 * Listing 5.13 read() and write() operations on the ByteBuf
 * <p>
 * Listing 5.14 Obtaining a ByteBufAllocator reference
 * <p>
 * Listing 5.15 Reference counting
 * <p>
 * Listing 5.16 Release reference-counted object
 */
public class ByteBufExamples {
    private final static Random random = new Random();
    private static final ByteBuf BYTE_BUF_FROM_SOMEWHERE = Unpooled.buffer(1024);
    private static final Channel CHANNEL_FROM_SOMEWHERE = new NioSocketChannel();
//    private static final ChannelHandlerContext CHANNEL_HANDLER_CONTEXT_FROM_SOMEWHERE = DUMMY_INSTANCE;

    /**
     * Listing 5.1 Backing array
     */
    public static void heapBuffer() {
        /*abstract class ByteBuf */
        ByteBuf heapBuf = BYTE_BUF_FROM_SOMEWHERE; //get reference form somewhere
        /*检查 ByteBuf 是否有一个支撑数组*/
        if (heapBuf.hasArray()) {
            /*如果有，则获取对该数组的引用*/
            byte[] array = heapBuf.array();
            /*计算第一个字节的偏移量。*/
            int offset = heapBuf.arrayOffset() + heapBuf.readerIndex();
            /*获得可读字节数*/
            int length = heapBuf.readableBytes();
            /*使用数组、偏移量和长度作为参数调用你的方法*/
            /*当 hasArray()方法返回 false 时，
            尝试访问支撑数组将触发一个 UnsupportedOperationException。
            这个模式类似于 JDK 的 ByteBuffer 的用法*/
            handleArray(array, offset, length);
        }
    }

    /**
     * Listing 5.2 Direct buffer data access
     */
    public static void directBuffer() {
        ByteBuf directBuf = BYTE_BUF_FROM_SOMEWHERE; //get reference form somewhere
        /*检查ByteBuf是否由数组支撑。如果不是，则这是一个直接缓冲区*/
        if (!directBuf.hasArray()) {
            /*获取可读字节数*/
            int length = directBuf.readableBytes();
            /*分配一个新的数组来保存具有该长度的字节数据*/
            byte[] array = new byte[length];
            /*将字节复制到该数组*/
            directBuf.getBytes(directBuf.readerIndex(), array);
            /*使用数组、偏移量和长度作为参数调用你的方法*/
            handleArray(array, 0, length);
        }
    }

    /**
     * Listing 5.3 Composite buffer pattern using ByteBuffer
     */
    public static void byteBufferComposite(ByteBuffer header, ByteBuffer body) {
        /*使用数组保存消息部分*/
        ByteBuffer[] message = new ByteBuffer[]{header, body};
        /*创建一个新的 ByteBuffer 并使用 copy 合并 header 和 body*/
        ByteBuffer message2 = ByteBuffer.allocate(header.remaining() + body.remaining());
        message2.put(header);
        message2.put(body);
        message2.flip();
    }


    /**
     * Listing 5.4 Composite buffer pattern using CompositeByteBuf
     */
    public static void byteBufComposite() {
        CompositeByteBuf messageBuf = Unpooled.compositeBuffer();
        /*头部*/
        ByteBuf headerBuf = BYTE_BUF_FROM_SOMEWHERE; // can be backing or direct
        /*主体*/
        ByteBuf bodyBuf = BYTE_BUF_FROM_SOMEWHERE;   // can be backing or direct

        /*将ByteBuf实例追加到CompositeByteBuf*/
        messageBuf.addComponents(headerBuf, bodyBuf);
        //...
        /*删除位于索引位置0的ByteBuf*/
        messageBuf.removeComponent(0); // remove the header
        for (ByteBuf buf : messageBuf) {
            System.out.println(buf.toString());
        }
    }

    /**
     * 访问 CompositeByteBuf 中的数据
     * Listing 5.5 Accessing the data in a CompositeByteBuf
     */
    public static void byteBufCompositeArray() {
        CompositeByteBuf compBuf = Unpooled.compositeBuffer();
        /*获得可读字节数*/
        int length = compBuf.readableBytes();
        /*分配一个具有可读字节数长度的新数组*/
        byte[] array = new byte[length];
        /*将字节读到该数组中*/
        compBuf.getBytes(compBuf.readerIndex(), array);
        /*使用偏移量和长度作为参数使用该数组*/
        handleArray(array, 0, array.length);
    }

    /**
     * 访问数据
     * Listing 5.6 Access data
     */
    public static void byteBufRelativeAccess() {
        ByteBuf buffer = BYTE_BUF_FROM_SOMEWHERE; //get reference form somewhere
        for (int i = 0; i < buffer.capacity(); i++) {
            byte b = buffer.getByte(i);
            System.out.println((char) b);
        }
    }

    /**
     * Listing 5.7 Read all data
     */
    public static void readAllData() {
        ByteBuf buffer = BYTE_BUF_FROM_SOMEWHERE; //get reference form somewhere
        while (buffer.isReadable()) {
            System.out.println(buffer.readByte());
        }
    }

    /**
     * Listing 5.8 Write data
     */
    public static void write() {
        // Fills the writable bytes of a buffer with random integers.
//        ByteBuf buffer = BYTE_BUF_FROM_SOMEWHERE; //get reference form somewhere
        ByteBuf buffer = PooledByteBufAllocator.DEFAULT.buffer(1024);
        while (buffer.writableBytes() >= 4) {
            buffer.writeInt(random.nextInt());
        }
    }

    /**
     * Listing 5.9 Using ByteProcessor to find \r
     * <p>
     * use {@link io.netty.buffer.ByteBufProcessor in Netty 4.0.x}
     */
    public static void byteProcessor() {
        ByteBuf buffer = BYTE_BUF_FROM_SOMEWHERE; //get reference form somewhere
        int index = buffer.forEachByte(ByteProcessor.FIND_CR);
    }

    /**
     * Listing 5.9 Using ByteBufProcessor to find \r
     * <p>
     * use {@link io.netty.util.ByteProcessor in Netty 4.1.x}
     */
    public static void byteBufProcessor() {
        ByteBuf buffer = BYTE_BUF_FROM_SOMEWHERE; //get reference form somewhere
        int index = buffer.forEachByte(ByteBufProcessor.FIND_CR);
    }

    /**
     * Listing 5.10 Slice a ByteBuf
     */
    public static void byteBufSlice() {
        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        ByteBuf sliced = buf.slice(0, 15);
        System.out.println(sliced.toString(utf8));
        buf.setByte(0, (byte) 'J');
        assert buf.getByte(0) == sliced.getByte(0);
    }

    /**
     * Listing 5.11 Copying a ByteBuf
     */
    public static void byteBufCopy() {
        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        ByteBuf copy = buf.copy(0, 15);
        System.out.println(copy.toString(utf8));
        buf.setByte(0, (byte) 'J');
        assert buf.getByte(0) != copy.getByte(0);
    }

    /**
     * Listing 5.12 get() and set() usage
     */

    public static void byteBufSetGet() {



        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        /*打印第一个字符，即N*/
        System.out.println((char) buf.getByte(0));
        /*当你从 ByteBuf 读取时，它的 readerIndex 将会被递增已经被读取的字节数。*/
        int readerIndex = buf.readerIndex();
        System.out.println(readerIndex);
        /*写入 ByteBuf 时，它的writerIndex 也会被递增。*/
        int writerIndex = buf.writerIndex();
        System.out.println(writerIndex);
//        System.out.println(Integer.MAX_VALUE);//62937
        /*将索引 0 处的字节更新为字符'B*/
        buf.setByte(0, (byte) 'B');
        /*打印第一个字符，即B*/
        System.out.println((char) buf.getByte(0));
          assert readerIndex == buf.readerIndex();
          assert writerIndex == buf.writerIndex();
    }

//    public static void main(String[] args) {
//        byteBufSetGet();
//        /**
//         * N
//         * 0
//         * 22
//         * B
//         */
//    }

    /**
     * Listing 5.13 read() and write() operations on the ByteBuf
     */
    public static void byteBufWriteRead() {
        Charset utf8 = Charset.forName("UTF-8");
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!", utf8);
        System.out.println((char) buf.readByte());
        int readerIndex = buf.readerIndex();
        System.out.println(readerIndex);
        int writerIndex = buf.writerIndex();
        System.out.println(writerIndex);
        buf.writeByte((byte) '?');
        System.out.println((char) buf.getByte(21));
        System.out.println((char) buf.getByte(22));
        assert readerIndex == buf.readerIndex();
        System.out.println(buf.readerIndex());
        assert writerIndex != buf.writerIndex();
        System.out.println(buf.writerIndex());
    }

//    public static void main(String[] args) {
//        byteBufWriteRead();
//    }

    private static void handleArray(byte[] array, int offset, int len) {
    }

    /**
     * Listing 5.14 Obtaining a ByteBufAllocator reference
     */
    public static void obtainingByteBufAllocatorReference() {
        Channel channel = CHANNEL_FROM_SOMEWHERE; //get reference form somewhere
        ByteBufAllocator allocator = channel.alloc();
        //...
//        ChannelHandlerContext ctx = CHANNEL_HANDLER_CONTEXT_FROM_SOMEWHERE; //get reference form somewhere
//        ByteBufAllocator allocator2 = ctx.alloc();
        //...
    }

    /**
     * Listing 5.15 Reference counting
     */
    public static void referenceCounting() {
        Channel channel = CHANNEL_FROM_SOMEWHERE; //get reference form somewhere
        /*从 Channel 获取ByteBufAllocator*/
        ByteBufAllocator allocator = channel.alloc();

       /* 从 ByteBufAllocator分配一个 ByteBuf*/
        ByteBuf buffer = allocator.directBuffer();

        buffer.release();// buffer.refCnt() == 0
//        assert buffer.refCnt() == 1;
        System.out.println(buffer.refCnt());
        //...
    }

    public static void main(String[] args) {
        referenceCounting();
    }

    /**
     * Listing 5.16 Release reference-counted object
     */
    public static void releaseReferenceCountedObject() {
        ByteBuf buffer = BYTE_BUF_FROM_SOMEWHERE; //get reference form somewhere
        boolean released = buffer.release();
        //...
    }




}
