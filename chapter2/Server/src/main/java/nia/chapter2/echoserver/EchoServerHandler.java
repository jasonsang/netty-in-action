package nia.chapter2.echoserver;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

/**
 * Listing 2.1 EchoServerHandler
 *
 * @author <a href="mailto:norman.maurer@gmail.com">Norman Maurer</a>
 */
@Sharable/*EchoServerHandler 被标注为@Shareable，所以我们可以总是使用同样的实例*/
public class EchoServerHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ByteBuf in = (ByteBuf) msg;
        System.out.println(
                "Server received: " + in.toString(CharsetUtil.UTF_8));
        ctx.write(in);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx)
            throws Exception {
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER)
                .addListener(ChannelFutureListener.CLOSE);
    }

    /*
    在Netty中，有两种发送消息的方式。你可以直接写到Channel中，
    也可以写到和ChannelHandler相关联的ChannelHandlerContext对象中。
    前一种方式将会导致消息从ChannelPipeline的尾端开始流动，
    而后者将导致消息从ChannelPipeline 中的下一个ChannelHandler开始流动。
    */

    /* 其代表了 ChannelHandler 和 ChannelPipeline 之间的绑定*/

    /*ChannelPipeline中的每个ChannelHandler
    将负责把事件转发到链中的下一个 ChannelHandler.
    这些适配器类（及它们的子类）将自动执行这个操作，
    所以你可以只重写那些你想要特殊处理的方法和事件。*/
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,
        Throwable cause) {
        cause.printStackTrace();

        /*更完善的应用会尝试从异常中恢复*/

        /*关闭连接来通知远程节点发生了错误*/
        ctx.close();
    }
}
