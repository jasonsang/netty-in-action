package nia.chapter2.echoserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;

/**
 * Listing 2.2 EchoServer class
 *
 * @author <a href="mailto:norman.maurer@gmail.com">Norman Maurer</a>
 */
public class EchoServer {
    private final int port;

    public EchoServer(int port) {
        this.port = port;
    }

    public static void main(String[] args)
        throws Exception {
//        if (args.length != 1) {
//            System.err.println("Usage: " + EchoServer.class.getSimpleName() +
//                " <port>"
//            );
//            return;
//        }
//        int port = Integer.parseInt(args[0]);
        int port = 1111;
        new EchoServer(port).start();
    }

    public void start() throws Exception {
        final EchoServerHandler serverHandler = new EchoServerHandler();
        /*包含一个或者多个EventLoop
        * 一个EventLoop在生命周期内只能和一个Thread绑定
        * 所有由EventLoop处理的I/O事件都将在它转悠的Thread上被处理
        * 一个Channel在它生命周期内只能注册于一个EventLoop
        * 一个EventLoop可能会被分配给一个或多个Channel*/
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            /*
            * 网络编程中的作用:绑定到一个本地端口
            * EventLoopGroup 的数目:2
            * 实际上，ServerBootstrap 类也可以只使用一个 EventLoopGroup，
            * 此时其将在两个场景下共用同一个 EventLoopGroup。
            * */
            ServerBootstrap b = new ServerBootstrap();
            b.group(group)
                .channel(NioServerSocketChannel.class)
                .localAddress(new InetSocketAddress(port))
                /*一个ChannelInitializer的实现被注册到了ServerBootstrap中*/
                .childHandler(new ChannelInitializer<SocketChannel>() {
                  /* ChannelInitializer.initChannel()方法被调用时，
                  ChannelInitializer 将在 ChannelPipeline
                  中安装一组自定义的 ChannelHandler；*/
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        /*。它们的执行顺序是由它们被添加的顺序所决定的。实际上，
                        被我们称为 ChannelPipeline 的是这些 ChannelHandler 的编排顺序*/


                        ch.pipeline().addLast(serverHandler);
                    }
                });

            /*异步地绑定服务器，调用sync()方法阻塞等待直到绑定完成*/
            ChannelFuture f = b.bind().sync();
            System.out.println(EchoServer.class.getName() +
                " started and listening for connections on " + f.channel().localAddress());
            /*获取Channel的CloseFuture，并且阻塞当前线程直到它完成*/
            f.channel().closeFuture().sync();
        } finally {
            /*关闭EventLoopGroup释放所有的资源*/
            group.shutdownGracefully().sync();
        }
    }
}
