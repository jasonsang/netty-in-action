package nia.chapter2.echoclient;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

/**
 * Listing 2.3 ChannelHandler for the client
 *
 * @author <a href="mailto:norman.maurer@gmail.com">Norman Maurer</a>
 */
@Sharable
/*<T> 是你要处理的消息的 Java 类型*/
public class EchoClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
    /**
     * 在到服务器的连接已经建立之后将被调用
     * @param ctx
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        /*当被通知 Channel是活跃的时候，发送一条消息*/
        ctx.writeAndFlush(Unpooled.copiedBuffer("Netty rocks!",
                CharsetUtil.UTF_8));
    }

    /**
     * 当从服务器接收到一条消息时被调用
     * @param ctx
     * @param in
     */
    /*不要阻塞当前的 I/O 线程*/
    @Override
    public void channelRead0(ChannelHandlerContext ctx, ByteBuf in) {

        /*并且获取一个到 ChannelHandlerContext 的引用，
        这个引用将作为输入参数传递给 ChannelHandler 的所有方法。*/
        Channel channel = ctx.channel();
        channel.write(Unpooled.copiedBuffer("Netty in Action",
                CharsetUtil.UTF_8));
        /*记录已接收消息的转储*/
        System.out.println(
                "Client received: " + in.toString(CharsetUtil.UTF_8));
        System.out.println(ctx.handler().getClass().getName());
        System.out.println(ctx.pipeline());
        System.out.println(ctx.read());
        ChannelFuture channelFuture = ctx.disconnect();
        System.out.println(channelFuture.isSuccess());

    }

    /**
     * 在处理过程中引发异常时被调用
     * @param ctx
     * @param cause
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,
        Throwable cause) {
        cause.printStackTrace();
        /*在发生异常时，记录错误并关闭Channel*/
        ctx.close();
    }
}
