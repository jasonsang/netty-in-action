package nia.chapter8;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;
import io.netty.util.concurrent.Future;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 * Listing 8.9 Graceful shutdown
 *
 * @author <a href="mailto:norman.maurer@gmail.com">Norman Maurer</a>
 * @author <a href="mailto:mawolfthal@gmail.com">Marvin Wolfthal</a>
 */
public class GracefulShutdown {
    public static void main(String args[]) {
        GracefulShutdown client = new GracefulShutdown();
        client.bootstrap();
    }

    /**
     * Listing 8.9 Graceful shutdown
     */
    public void bootstrap() {
        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
             .channel(NioSocketChannel.class)
        //...
             .handler(
                new SimpleChannelInboundHandler<ByteBuf>() {
                    @Override
                    public void channelActive(ChannelHandlerContext ctx) throws Exception {
                        super.channelActive(ctx);
                        System.out.println("客户端与服务端通道-开启：" + ctx.channel().localAddress() + "channelActive");
//                        log.info("客户端与服务端通道-开启：" + ctx.channel().localAddress() + "channelActive");
//        toCode = "<alarm><req>yes</req><auto>yes</auto></alarm>\n";

                        ctx.writeAndFlush(Unpooled.copiedBuffer("FF 08 11 00 01 00 00 00 08 A5 73  ", CharsetUtil.UTF_8)); // 必须有flush

                    }

                    @Override
                    protected void channelRead0(
                            ChannelHandlerContext channelHandlerContext,
                            ByteBuf byteBuf) throws Exception {
                        System.out.println("Received data");

                        ByteBuf buf = byteBuf.readBytes(byteBuf.readableBytes());
                        System.out.println(
                         "客户端接收到的服务端信息:"
                         + ByteBufUtil.hexDump(buf)
                         + "; 数据包为:"
                         + buf.toString(Charset.forName("gbk")));//解析返回值
                        Document doc = Jsoup.parse(buf.toString(Charset.forName("gbk")));//html为内容
//        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.DISABLED);
                        ReferenceCountUtil.release(buf);
                    }
                }
             );
        bootstrap.connect(new InetSocketAddress("10.2.176.228", 3001)).syncUninterruptibly();
        //,,,
        Future<?> future = group.shutdownGracefully();
        // block until the group has shutdown
        future.syncUninterruptibly();
    }
}
