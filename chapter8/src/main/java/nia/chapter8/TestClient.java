package nia.chapter8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
 
public class TestClient {
	public static void main(String[] args) {
		new MiniClient("10.2.176.228", 3001);
	}
}
class MiniClient {
	private SocketChannel sc;
	private ByteBuffer w_bBuf;
	private ByteBuffer r_bBuf = ByteBuffer.allocate(1024);
	public MiniClient(String host, int port) {
		try {
			InetSocketAddress remote = new InetSocketAddress(host, port);
			sc = SocketChannel.open();
			sc.connect(remote);
			if(sc.finishConnect()) {
				System.out.println("已经与服务器成功建立连接...");
			}
			while(true) {
				if(!sc.socket().isConnected()) {
					System.out.println("已经与服务器失去了连接...");
					return ;
				}
//				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//				String str = br.readLine();
//				System.out.println("读入一行数据，开始发送...");
//				FF 08 11 00 01 00 00 00 08 A5 73
				byte [] Delivery_Conf = {(byte)0xFF, (byte)0x08, (byte)0x11,
						(byte)0x00,
						(byte)0x01,
						(byte)0x00,
						(byte)0x00,
						(byte)0x00,
						(byte)0x08,
						(byte)0xA5,
						(byte)0x73};
// ...
//				w_bBuf = ByteBuffer.wrap(str.getBytes("UTF-8"));
				//将缓冲区中数据写入通道
//				sc.write(w_bBuf);
//				sc.write(Delivery_Conf);
				System.out.println("数据发送成功...");
				w_bBuf.clear();
				System.out.println("接收服务器端响应消息...");
				try {
					Thread.currentThread();
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				r_bBuf.clear();
				//将字节序列从此通道中读入给定的缓冲区r_bBuf
				sc.read(r_bBuf);
				r_bBuf.flip();
				String msg = Charset.forName("UTF-8").decode(r_bBuf).toString();
				System.out.println(msg);				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
