package nia.chapter8;


import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.scheduling.annotation.EnableAsync;

import java.nio.charset.Charset;
import java.util.SortedMap;
import java.util.TreeMap;

@Slf4j(topic = "监听机床信息类")
@EnableAsync
public class EchoAlarmHandler extends SimpleChannelInboundHandler<ByteBuf> {

    static final String CODE = "";

    /**
     * 向服务端发送数据
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx)  {
        log.info("客户端与服务端通道-开启：" + ctx.channel().localAddress() + "channelActive");
//        toCode = "<alarm><req>yes</req><auto>yes</auto></alarm>\n";
        //System.out.println("客户端准备发送的数据包：" + toCode);
//        ctx.writeAndFlush(Unpooled.copiedBuffer(CODE, CharsetUtil.UTF_8)); // 必须有flush
    }

    /**
     * channelInactive
     * <p>
     * channel 通道 Inactive 不活跃的
     * <p>
     * 当客户端主动断开服务端的链接后，这个通道就是不活跃的。也就是说客户端与服务端的关闭了通信通道并且不可以传输数据
     */
//    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
//        log.info("客户端与服务端通道-关闭："
//                + ctx.channel().localAddress()
//                + "channelInactive");
//    }
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("客户端与服务端通道-关闭："
                + ctx.channel().localAddress()
                + "channelInactive");

//        if(reconnect){
//            System.out.println("链接关闭，将进行重连");
//            if (attempts < 12) {
//                attempts++;
//                //重连的间隔时间会越来越长
//                int timeout = 2 << attempts;
//                timer.newTimeout(this, timeout, TimeUnit.MILLISECONDS);
//            }
//        }
        log.error("正在重连...");
//        cncAlarm.doFirst();
        ctx.fireChannelInactive();

        /*默认情况下 adapter 会通过 ctx.fireChannelInactive()
        方法直接把上一个 handler 的输出结果传递到下一个 handler。*/
//        ctx.fireChannelInactive();
    }
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        //System.out.println("读取客户端通道信息..");

//        ByteBuf buff=(ByteBuf) msg;
//        byte[] req=new byte[buff.readableBytes()];

        ByteBuf buf = msg.readBytes(msg.readableBytes());
        /**System.out.println(
                "客户端接收到的服务端信息:"
                        + ByteBufUtil.hexDump(buf)
                        + "; 数据包为:"
                        + buf.toString(Charset.forName("gbk")));*//*解析返回值*/
        Document doc = Jsoup.parse(buf.toString(Charset.forName("gbk")));//html为内容
//        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.DISABLED);
//        ReferenceCountUtil.release(buf);


    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
        System.out.println("异常退出:" + cause.getMessage());
//        log.error("正在重连...");
//        cncAlarm.doFirst();

    }


}