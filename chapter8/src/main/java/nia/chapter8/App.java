package nia.chapter8;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import static nia.chapter8.ByteUtil.Bytes2HexString;
import static nia.chapter8.ByteUtil.HexString2Bytes;

/**
 * Socket通信
 *
 */
public class App
{
    public static void main( String[] args )
    {
        Socket socket = null;
        String strReturn = null;
        try {
            System.out.println("connecting...");
            socket = new Socket("10.2.176.228", 3001);
            System.out.println("connection success");
            while (true){
                String str = "FF 08 11 00 01 00 00 00 08 A5 73"; //发送的16进制字符串
                byte[] bytes = hexStringToByteArray(str);
                System.out.println(bytes);
                OutputStream os = socket.getOutputStream();
                os.write(bytes);
                //输入流
                InputStream in=socket.getInputStream();
                //接收服务器的响应
                int line = 0;
                byte[] buf = new byte[100];
                //接收收到的数据
                int len = in.read(buf);
                strReturn= BinaryToHexString(buf);
                byte[] b = HexString2Bytes(strReturn.trim().replace(" ",""));

//
//                String s = strReturn;
                String hex = HexStringUtils.toHexString(b);

                System.out.println("原字符串:" + hex);

                System.out.println("还原:" +strReturn );
                StringBuffer sbResult = new StringBuffer(hex); //String转换为StringBuffer
//
//                String b2 = "0x323334357372722C";
                byte[] bytes2 = hexStringToByteArray("0x"+sbResult);
                String st = new String(bytes2, StandardCharsets.UTF_8);
//                System.out.println(st);
            System.out.println(Thread.currentThread().getName() + "获取返回的数据:" + st);
                //os.close();
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {

                }
            }
        }
    }

    /**
     * 16进制表示的字符串转换为字节数组
     *
     * @param hexString 16进制表示的字符串
     * @return byte[] 字节数组
     */
    public static byte[] hexStringToByteArray(String hexString) {
        hexString = hexString.replaceAll(" ", "");
        int len = hexString.length();
        byte[] bytes = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            // 两位一组，表示一个字节,把这样表示的16进制字符串，还原成一个字节
            bytes[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character
                    .digit(hexString.charAt(i + 1), 16));
        }
        return bytes;
    }

    /**
     * 将字节数组转换成十六进制的字符串
     *
     * @return
     */
    public static String BinaryToHexString(byte[] bytes) {
        String hexStr = "0123456789ABCDEF";
        String result = "";
        String hex = "";
        for (byte b : bytes) {
            hex = String.valueOf(hexStr.charAt((b & 0xF0) >> 4));
            hex += String.valueOf(hexStr.charAt(b & 0x0F));
            result += hex + " ";
        }
        return result;
    }

}
