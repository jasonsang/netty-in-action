package nio;


import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

/**
 * @desc 基于JAVA NIO 实现的 Socket 文件传输--客户端
 **/


public class TestNio
{
//    private static final Logger log = LoggerFactory.getLogger(nio.TestNio.class);




    public static void main(String[] args)
    {
        SocketChannel sc = null;
        FileChannel fc = null;
        try
        {
            sc = SocketChannel.open();
            // client 设置阻塞模式
//            sc.configureBlocking(true);
            sc.connect(new InetSocketAddress("10.2.52.11", 62937));
            if (!sc.finishConnect())
            {
                System.out.println("Can not connect to server");
                return;
            }
            // 分配缓冲区【从堆内申请内存】
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            int r = 0;
            //从文件读取内容，并通过 Socket 发送
            fc = new FileInputStream(new File(TestNio.class.getClassLoader().getResource("cncCode/1.txt").toURI())).getChannel();
//            fc = new
            try
            {
                // 先发送此文件的大小
                buffer.putLong(fc.size());
                while ((r = fc.read(buffer)) > 0)
                {
                    System.out.println("Read bytes from file"+r);
                    /**
                     * 解释： 缓冲区中的 position 表示“下一次读或者写的位置（数组下标）”
                     *                   limit 表示“可读或者可写的最大位置”
                     *                   capacity 表示“缓冲区数组的总长度，只读”
                     *
                     *        缓冲区新建的时候： position = 0 , limit = capacity
                     *        在缓冲区写入 n 个字节后， position 向后移动 n 位 ，即 position = n , limit = capacity
                     *
                     * 设置 limit 为 position 的值 r ， 再将 position 设置为 0 【调用 flip() 即可实现 】
                     */
                    buffer.flip();

                    /**
                     * 将 buffer 中的内容全部发送出去。【将 buffer 中的数据写入 Channel 中】
                     * buffer.hasRemaining()  判断缓冲区中是否还有数据
                     */
                    while (buffer.hasRemaining() && (r = sc.write(buffer)) > 0)
                    {
//                        log.debug("Write {} bytes to server", r);
                    }
                    /**
                     * 使用  clear() 之后，  缓冲区中的 position = 0 , limit = capacity
                     */
                    buffer.clear();
                }
            }
            finally
            {
//                StreamUtil.close(fc);
            }

            // 读取服务端返回的响应字符串【将 Channel 中的数据读到 buffer 中】
            while ( (r = sc.read(buffer)) > 0)
            {
//                log.debug("Read {} bytes from socket",r);
            }
            buffer.flip();
            // 解码服务端返回的数据
//            log.info(Charset.forName("UTF-8").decode(buffer).toString());
        }
        catch (IOException | URISyntaxException e)
        {
//            log.error("Error on send file", e);
        }
        finally
        {
//            StreamUtil.close(sc);
        }
        System.out.println("done");
    }
}

