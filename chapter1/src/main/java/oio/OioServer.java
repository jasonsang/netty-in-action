package oio;


import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OioServer {
    public static void main(String[] args) throws IOException {
        /*创建线程池*/
        ExecutorService executorService = Executors.newCachedThreadPool();
        /*创建Socket服务*/
        ServerSocket serverSocket = new ServerSocket(1990);
        System.out.println("--- 服务器启动 ---");
        while (true) {
            /*第一个阻塞点 获取一个套接字*/
            /*每一个客户端接入消耗一个Socket会浪费资源*/
            final Socket socket = serverSocket.accept();
            /*可以用telnet工具测试 telnet 127.0.0.1 1990 */
            System.out.println("-->来一个新客户端");
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    /*业务处理*/
                    handler(socket);
                }
            });
        }
    }

    /**
     * 读取数据
     */
    private static void handler(Socket socket) {
        try {
            byte[] bytes = new byte[1024];
            InputStream inputStream = socket.getInputStream();
            while (true) {
                /*读取数据（阻塞）*/
                int read = inputStream.read(bytes);
                if (read != -1) {
                    System.out.println(new String(bytes,0,read));
                } else {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                System.out.println("socket关闭");
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
